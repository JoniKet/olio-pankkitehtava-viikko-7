/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi.viikko.pkg7.pankkiohjelma;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Jönnsson
 */
public class Bank {
    protected String tilinumero;
    Scanner scan = new Scanner(System.in);
    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    ArrayList<Account> Tilit = new ArrayList<Account>();
    
    public Bank(){
    }
    
    public void removeTilinumero() throws IOException{
        System.out.print("Syötä poistettava tilinumero: ");
        String etsittävä = br.readLine();
        Account a = findAccount(etsittävä);
        if( a == null){
            System.out.println("Tiliä ei löydy");
        }
        else{
            Tilit.remove(a);
            System.out.println("Tili poistettu.");
       
        }
    }
    public Account findAccount(String etsittävä) throws IOException{
            Account b;
            for(Account a : Tilit){
                if (a.tilinumero == null ? etsittävä == null : a.tilinumero.equals(etsittävä)){
                    return a;
                }
            }
            b = null;
            return b;
        }
    
    public void printAccount() throws IOException{
        System.out.print("Syötä tulostettava tilinumero: ");
        String tempTilinumero = br.readLine();
        Account a = findAccount(tempTilinumero);
        if( a == null){
            System.out.println("Tiliä ei löydy!");
        }
        else{
            System.out.println("Tilinumero: " + a.tilinumero + " Tilillä rahaa: "+a.saldo);
        }
    }
    
    public void nostaAccount() throws IOException{
        System.out.print("Syötä tilinumero: ");
        String tempTilinumero = br.readLine();
        Account a = findAccount(tempTilinumero);
        if( a == null){
            System.out.println("Tiliä ei löydy!");
        }
        else{
            System.out.print("Syötä rahamäärä: ");
            int tempnosto = scan.nextInt();
            if(tempnosto < (a.saldo + a.luottoRaja)){
                a.saldo = a.saldo - tempnosto;
            }
            else{
                System.out.println("Tilillä ei riitä saldo!");
            }
        }
        
    }
    
    public void talletaAccount() throws IOException{
        System.out.print("Syötä tilinumero: ");
        String tempTilinumero = br.readLine();
        Account a = findAccount(tempTilinumero);
        if( a == null){
            System.out.println("Tiliä ei löydy!");
        }
        else{
            System.out.print("Syötä rahamäärä: ");
            int tempnosto = scan.nextInt();
            a.saldo = a.saldo + tempnosto;
        }
    }
    
    public void listAccounts(){
        System.out.println("Kaikki tilit:");
        for(Account a : Tilit){
            System.out.print("Tilinumero: "+ a.tilinumero + " Tilillä rahaa: "+a.saldo);
            if(a.luottoRaja > 0){
                System.out.println(" Luottoraja: "+a.luottoRaja);
            }
            else{
                System.out.println();
            }
        }
    }
    
}   
    
    


