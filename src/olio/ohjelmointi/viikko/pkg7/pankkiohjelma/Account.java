/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi.viikko.pkg7.pankkiohjelma;

import java.io.IOException;

/**
 *
 * @author Jönnsson
 */
public abstract class Account extends Bank {
    protected int saldo;
    protected int luottoRaja; 

    public Account() throws IOException{
        System.out.print("Syötä tilinumero: ");
        tilinumero = br.readLine();
        System.out.print("Syötä rahamäärä: ");
        saldo = scan.nextInt();
    }
    
    public Account lisäys(){
        System.out.println("Tili luotu.");
        return this;
    }
}

class NormalAccount extends Account{
    
    public NormalAccount() throws IOException{
    }
}

class LuottoTili extends Account{           
    public LuottoTili() throws IOException{
        System.out.print("Syötä luottoraja: ");
        luottoRaja = scan.nextInt();

    }  
}    