/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package olio.ohjelmointi.viikko.pkg7.pankkiohjelma;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Jönnsson
 */
public class Mainclass {
    /**
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        int valinta = 9001;
        Scanner scan = new Scanner(System.in);
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String tilinumero;
        int saldo;
        int luottoraja;
        Bank bank = new Bank() {};
        while(valinta != 0){
            Valikko();
            valinta = scan.nextInt();
            if(valinta == 1){
                    NormalAccount normalAccount = new NormalAccount();
                    Account temp = normalAccount.lisäys();
                    bank.Tilit.add(temp);
            }
            else if(valinta == 2){
                    LuottoTili luottoTili = new LuottoTili();
                    Account temp = luottoTili.lisäys();
                    bank.Tilit.add(temp);
                    }
            else if(valinta == 3){
                    bank.talletaAccount();
                    }
            else if(valinta == 4){
                    bank.nostaAccount();
                    }
            else if(valinta == 5){
                    bank.removeTilinumero();
                    }
            else if(valinta == 6){
                    bank.printAccount();
                    }
            else if(valinta == 7){
                    bank.listAccounts();
                    }
            else if(valinta == 0){
                    }
            else{
                System.out.println("Valinta ei kelpaa!");

            }
        }
        
    }
    
    
    public static void Valikko(){
        System.out.println();
        System.out.println("*** PANKKIJÄRJESTELMÄ ***");
        System.out.println("1) Lisää tavallinen tili");
        System.out.println("2) Lisää luotollinen tili");
        System.out.println("3) Tallenna tilille rahaa");
        System.out.println("4) Nosta tililtä");
        System.out.println("5) Poista tili");
        System.out.println("6) Tulosta tili");
        System.out.println("7) Tulosta kaikki tilit");
        System.out.println("0) Lopeta");
        System.out.print("Valintasi: ");
    }
    
}
